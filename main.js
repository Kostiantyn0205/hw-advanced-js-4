/*AJAX дозволяє веб-додаткам звертатися до сервера без перезавантаження сторінки. Це дозволяє створити швидку веб-сторінку, яка буде реагують на дії користувачів миттєво,
зменшуючи завантаження сервера. Також AJAX дозволяє взаємодіяти з сервером асинхронно.*/

const filmList = document.getElementById('filmList');

function getFilms() {
    fetch('https://ajax.test-danit.com/api/swapi/films')
        .then(response => response.json())
        .then(data => {
            data.forEach(film => {
                const filmListItem = document.createElement('li');
                filmListItem.innerHTML = `<strong>Епізод ${film.episodeId}</strong>: ${film.name} - ${film.openingCrawl}`;
                filmList.appendChild(filmListItem);

                const charactersContainerForFilm = document.createElement('div');
                charactersContainerForFilm.innerHTML = `<strong>Персонажи:</strong>`;
                filmListItem.appendChild(charactersContainerForFilm);

                getCharacters(film.characters, charactersContainerForFilm);
            });
        })
        .catch(error => console.error('Помилка отримання списку фільмів:', error));
}

function getCharacters(charactersURLs, container) {
    const loadingAnimation = document.createElement('div');
    loadingAnimation.classList.add('spinner');
    container.appendChild(loadingAnimation);

    Promise.all(
        charactersURLs.map(url =>
            fetch(url)
                .then(response => response.json())
        )
    )
        .then(characters => {
            loadingAnimation.style.display = 'none';

            characters.forEach(character => {
                const characterInfo = document.createElement('div');
                characterInfo.innerHTML = `Ім'я: ${character.name}; `;
                container.appendChild(characterInfo);
            });
        })
        .catch(error => console.error('Помилка отримання персонажів:', error));
}

getFilms();